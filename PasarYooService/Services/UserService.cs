﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using PasarYooService.DTOs;
using PasarYooService.Interfaces;
using PasarYooService.Models;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace PasarYooService.Services
{
    public class UserService : IUserService
    {
        private readonly UnitOfWork _unitOfWork;
        private readonly ILogger _logger;
        private readonly ClaimsPrincipal _user;

        public UserService(ILogger<UserService> logger, PasarYooServiceContext context, IHttpContextAccessor httpContextAccessor)
        {
            _logger = logger;
            _user = httpContextAccessor?.HttpContext?.User;
            _unitOfWork = new UnitOfWork(context);
        }

        public async Task<User> GetAuthorizedUser()
        {
            var userId = Guid.Parse(_user.Claims.FirstOrDefault(i => i.Type == ClaimTypes.Name)?.Value);

            var user = await _unitOfWork.UserRepository.GetAll()
                .Where(u => u.UserId == userId)
                .FirstOrDefaultAsync();

            if (user == null)
            {
                throw new Exception("you are not authorized.");
            }

            return user;
        }

        public async Task<User> AuthenticateAsync(string username, string password)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
                return null;

            var user = await _unitOfWork.UserRepository
                    .GetAll()
                    .Where(x => x.Username.ToLower() == username.ToLower() && x.BlockedStatus != true)
                    .FirstOrDefaultAsync();

            // check if username exists
            if (user == null)
                return null;

            // check if password is correct
            if (BCrypt.CheckPassword(password, user.PasswordHash))
            {
                return user;
            }

            return null;
        }

        public async Task<User> GetById(Guid UserId)
        {
            var user = await _unitOfWork.UserRepository.GetAll()
                .Where(u => u.UserId == UserId && u.BlockedStatus != true)
                .FirstOrDefaultAsync();

            return user;
        }

        public async Task<LoginResponse> Login(Login user)
        {
            try
            {
                var loginUser = await AuthenticateAsync(user.Username, user.Password);

                if (loginUser == null)
                {
                    throw new Exception("username or password is incorrect.");
                }

                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(Constants.TokenDescriptorKey);
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                    new Claim(ClaimTypes.Name, loginUser.UserId.ToString())
                    }),
                    Expires = DateTime.UtcNow.AddYears(1),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                };
                var token = tokenHandler.CreateToken(tokenDescriptor);
                var tokenString = tokenHandler.WriteToken(token);
                var result = new LoginResponse()
                {
                    AccessToken = tokenString,
                    Firstname = loginUser.Firstname,
                    UserId = loginUser.UserId,
                    Username = loginUser.Username
                };

                // return basic user info (without password) and token to store client side
                return result;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, e);
                throw e;
            }
        }

        public bool CheckUsername(string username)
        {
            var result = _unitOfWork.UserRepository.IsExist(u => u.Username == username);

            return result;
        }

        public async Task Register(RegisterDTO user)
        {
            try
            {
                var checkUser = await _unitOfWork.UserRepository.GetAll()
                    .Where(u => u.Username == user.Username || u.PhoneNumber == user.PhoneNumber || u.Email == u.Email)
                    .FirstOrDefaultAsync();

                if(checkUser != null)
                {
                    throw new Exception("User already exists.");
                }

                var passwordSalt = BCrypt.GenerateSalt();
                checkUser = new User
                {
                    Email = user.Email,
                    Firstname = user.Firstname,
                    Lastname = user.Lastname,
                    PhoneNumber = user.PhoneNumber,
                    UpdatedBy = Constants.SystemName,
                    UpdatedDate = DateTime.UtcNow.AddHours(7),
                    Username = user.Username,
                    RegistrationDate = DateTime.UtcNow.AddHours(7),
                    PasswordSalt = passwordSalt,
                    PasswordHash = BCrypt.HashPassword(user.Password, passwordSalt),
                };

                _unitOfWork.UserRepository.Add(checkUser);
                await _unitOfWork.SaveAsync();
            }
            catch(Exception e)
            {
                _logger.LogError(e.Message, e);
                throw e;
            }
        }
    }
}
