﻿using Microsoft.EntityFrameworkCore;
using PasarYooService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PasarYooService.Repositories
{
    public class MarketRepository : BaseRepository<Market>
    {
        public MarketRepository(DbContext dbContext) : base(dbContext)
        {
        }
    }
}
