﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PasarYooService.Models
{
    public class Region
    {
        [Key]
        public Guid RegionId { get; set; }

        [StringLength(10)]
        public string RegionCode { get; set; }

        [StringLength(100)]
        public string RegionDescription { get; set; }

        [StringLength(100)]
        public string Latitude { get; set; }

        [StringLength(100)]
        public string Longitude { get; set; }
    }
}
