﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PasarYooService.Models
{
    public class Store
    {
        [Key]
        public Guid StoreId { get; set; }

        public Guid MarketId { get; set; }
        public Market Market { get; set; }

        public Guid UserId { get; set; }

        [StringLength(100)]
        public string StoreName { get; set; }

        public int Like { get; set; }

        public bool ClosedStatus { get; set; }

        [StringLength(1000)]
        public string Description { get; set; }

        public string Photo { get; set; }

        [StringLength(100)]
        public string UpdatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }
    }
}
