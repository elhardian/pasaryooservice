﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PasarYooService.Models
{
    public class Market
    {
        [Key]
        public Guid MarketId { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(1000)]
        public string Description { get; set; }

        [StringLength(1000)]
        public string Address { get; set; }

        [StringLength(100)]
        public string Latitude { get; set; }

        [StringLength(100)]
        public string Longitude { get; set; }

        public string Photo { get; set; }

        [StringLength(100)]
        public string UpdatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }
    }
}
