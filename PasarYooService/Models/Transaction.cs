﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PasarYooService.Models
{
    public class Transaction
    {
        [Key]
        public Guid TransactionId { get; set; }

        public DateTime? TransactionDate { get; set; }

        public int Total { get; set; }

        [StringLength(100)]
        public string DestinationLatitude { get; set; }

        [StringLength(100)]
        public string DestinationLongitude { get; set; }

        public int Status { get; set; }

        [StringLength(10)]
        public string PaymentMethod { get; set; }

        public DateTime? TransactionClosedDate { get; set; }

        public DateTime? TransactionRejectedDate { get; set; }

        [StringLength(1000)]
        public string TransactionRejectedReason { get; set; }

        [StringLength(1000)]
        public string Note { get; set; }

        public Guid? DriverId { get; set; }

        public Driver Driver { get; set; }

        public Guid UserId { get; set; }
        public User User { get; set; }
    }
}
