﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PasarYooService.Models
{
    public class TransactionProduct
    {
        [Key]
        public Guid TransactionProductId { get; set; }

        public string ProductName { get; set; }

        public int Quantity { get; set; }

        public int Price { get; set; }

        [StringLength(100)]
        public string PriceInfo { get; set; }

        public Guid TransactionId { get; set; }

        public Transaction Transaction { get; set; }

        public Guid ProductId { get; set; }
        public Product Product { get; set; }
    }
}
