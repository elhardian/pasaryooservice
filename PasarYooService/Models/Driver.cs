﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PasarYooService.Models
{
    public class Driver
    {
        [Key]
        public Guid DriverId { get; set; }

        public Guid UserId { get; set; }
        public User User { get; set; }

        public Guid RegionId { get; set; }
        public Region Region { get; set; }

        [StringLength(100)]
        public string NIK { get; set; }

        [StringLength(100)]
        public string PhoneNumber { get; set; }

        [StringLength(100)]
        public string UpdatedBy { get; set; }

        public string Photo { get; set; }

        public string PlatNumber { get; set; }

        public DateTime UpdatedDate { get; set; }

        [StringLength(100)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
