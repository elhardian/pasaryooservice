﻿using PasarYooService.DTOs;
using PasarYooService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PasarYooService.Interfaces
{
    public interface IUserService
    {
        Task<User> AuthenticateAsync(string username, string password);
        Task<User> GetAuthorizedUser();
        Task<User> GetById(Guid UserId);
        Task<LoginResponse> Login(Login user);
        bool CheckUsername(string username);
        Task Register(RegisterDTO user);
    }
}
