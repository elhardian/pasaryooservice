﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using PasarYooService.Models;

namespace PasarYooService.Migrations
{
    [DbContext(typeof(PasarYooServiceContext))]
    [Migration("20200425163334_Initial")]
    partial class Initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.6-servicing-10079")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("PasarYooService.Models.Driver", b =>
                {
                    b.Property<Guid>("DriverId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy")
                        .HasMaxLength(100);

                    b.Property<DateTime>("CreatedDate");

                    b.Property<string>("NIK")
                        .HasMaxLength(100);

                    b.Property<string>("PhoneNumber")
                        .HasMaxLength(100);

                    b.Property<string>("Photo");

                    b.Property<string>("PlatNumber");

                    b.Property<Guid>("RegionId");

                    b.Property<string>("UpdatedBy")
                        .HasMaxLength(100);

                    b.Property<DateTime>("UpdatedDate");

                    b.Property<Guid>("UserId");

                    b.HasKey("DriverId");

                    b.HasIndex("RegionId");

                    b.HasIndex("UserId");

                    b.ToTable("Drivers");
                });

            modelBuilder.Entity("PasarYooService.Models.Market", b =>
                {
                    b.Property<Guid>("MarketId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address")
                        .HasMaxLength(1000);

                    b.Property<string>("Description")
                        .HasMaxLength(1000);

                    b.Property<string>("Latitude")
                        .HasMaxLength(100);

                    b.Property<string>("Longitude")
                        .HasMaxLength(100);

                    b.Property<string>("Name")
                        .HasMaxLength(100);

                    b.Property<string>("Photo");

                    b.Property<string>("UpdatedBy")
                        .HasMaxLength(100);

                    b.Property<DateTime>("UpdatedDate");

                    b.HasKey("MarketId");

                    b.ToTable("Markets");
                });

            modelBuilder.Entity("PasarYooService.Models.Product", b =>
                {
                    b.Property<Guid>("ProductId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy")
                        .HasMaxLength(100);

                    b.Property<DateTime>("CreatedDate");

                    b.Property<string>("Description")
                        .HasMaxLength(1000);

                    b.Property<string>("Name")
                        .HasMaxLength(100);

                    b.Property<int>("Price");

                    b.Property<string>("PriceInfo")
                        .HasMaxLength(100);

                    b.Property<bool>("Status");

                    b.Property<bool>("StockStatus");

                    b.Property<Guid>("StoreId");

                    b.Property<string>("UpdatedBy");

                    b.Property<DateTime>("UpdatedDate");

                    b.HasKey("ProductId");

                    b.HasIndex("StoreId");

                    b.ToTable("Products");
                });

            modelBuilder.Entity("PasarYooService.Models.Region", b =>
                {
                    b.Property<Guid>("RegionId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Latitude")
                        .HasMaxLength(100);

                    b.Property<string>("Longitude")
                        .HasMaxLength(100);

                    b.Property<string>("RegionCode")
                        .HasMaxLength(10);

                    b.Property<string>("RegionDescription")
                        .HasMaxLength(100);

                    b.HasKey("RegionId");

                    b.ToTable("Regions");
                });

            modelBuilder.Entity("PasarYooService.Models.Store", b =>
                {
                    b.Property<Guid>("StoreId")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("ClosedStatus");

                    b.Property<string>("Description")
                        .HasMaxLength(1000);

                    b.Property<int>("Like");

                    b.Property<Guid>("MarketId");

                    b.Property<string>("Photo");

                    b.Property<string>("StoreName")
                        .HasMaxLength(100);

                    b.Property<string>("UpdatedBy")
                        .HasMaxLength(100);

                    b.Property<DateTime>("UpdatedDate");

                    b.Property<Guid>("UserId");

                    b.HasKey("StoreId");

                    b.HasIndex("MarketId");

                    b.ToTable("Stores");
                });

            modelBuilder.Entity("PasarYooService.Models.Transaction", b =>
                {
                    b.Property<Guid>("TransactionId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("DestinationLatitude")
                        .HasMaxLength(100);

                    b.Property<string>("DestinationLongitude")
                        .HasMaxLength(100);

                    b.Property<Guid?>("DriverId");

                    b.Property<string>("Note")
                        .HasMaxLength(1000);

                    b.Property<string>("PaymentMethod")
                        .HasMaxLength(10);

                    b.Property<int>("Status");

                    b.Property<int>("Total");

                    b.Property<DateTime?>("TransactionClosedDate");

                    b.Property<DateTime?>("TransactionDate");

                    b.Property<DateTime?>("TransactionRejectedDate");

                    b.Property<string>("TransactionRejectedReason")
                        .HasMaxLength(1000);

                    b.Property<Guid>("UserId");

                    b.HasKey("TransactionId");

                    b.HasIndex("DriverId");

                    b.HasIndex("UserId");

                    b.ToTable("Transactions");
                });

            modelBuilder.Entity("PasarYooService.Models.TransactionProduct", b =>
                {
                    b.Property<Guid>("TransactionProductId")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("Price");

                    b.Property<string>("PriceInfo")
                        .HasMaxLength(100);

                    b.Property<Guid>("ProductId");

                    b.Property<string>("ProductName");

                    b.Property<int>("Quantity");

                    b.Property<Guid>("TransactionId");

                    b.HasKey("TransactionProductId");

                    b.HasIndex("ProductId");

                    b.HasIndex("TransactionId");

                    b.ToTable("TransactionProducts");
                });

            modelBuilder.Entity("PasarYooService.Models.User", b =>
                {
                    b.Property<Guid>("UserId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("BlockedReason")
                        .HasMaxLength(1000);

                    b.Property<bool?>("BlockedStatus");

                    b.Property<string>("Email")
                        .HasMaxLength(150);

                    b.Property<string>("Firstname")
                        .HasMaxLength(250);

                    b.Property<string>("ForgotPasswordCode")
                        .HasMaxLength(10);

                    b.Property<string>("Lastname")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<string>("PasswordHash")
                        .HasMaxLength(1000);

                    b.Property<string>("PasswordSalt")
                        .HasMaxLength(1000);

                    b.Property<string>("PhoneNumber")
                        .IsRequired()
                        .HasMaxLength(20);

                    b.Property<DateTime>("RegistrationDate");

                    b.Property<string>("UpdatedBy")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<DateTime>("UpdatedDate");

                    b.Property<string>("Username")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.HasKey("UserId");

                    b.ToTable("Users");
                });

            modelBuilder.Entity("PasarYooService.Models.Driver", b =>
                {
                    b.HasOne("PasarYooService.Models.Region", "Region")
                        .WithMany()
                        .HasForeignKey("RegionId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("PasarYooService.Models.User", "User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("PasarYooService.Models.Product", b =>
                {
                    b.HasOne("PasarYooService.Models.Store", "Store")
                        .WithMany()
                        .HasForeignKey("StoreId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("PasarYooService.Models.Store", b =>
                {
                    b.HasOne("PasarYooService.Models.Market", "Market")
                        .WithMany()
                        .HasForeignKey("MarketId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("PasarYooService.Models.Transaction", b =>
                {
                    b.HasOne("PasarYooService.Models.Driver", "Driver")
                        .WithMany()
                        .HasForeignKey("DriverId");

                    b.HasOne("PasarYooService.Models.User", "User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("PasarYooService.Models.TransactionProduct", b =>
                {
                    b.HasOne("PasarYooService.Models.Product", "Product")
                        .WithMany()
                        .HasForeignKey("ProductId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("PasarYooService.Models.Transaction", "Transaction")
                        .WithMany()
                        .HasForeignKey("TransactionId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
